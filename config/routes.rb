Rails.application.routes.draw do
  resources :charges, only: %i(show create destroy)
  resources :transactions, only: %i(show create destroy)
  resources :companies
  resources :people, except: %i(destroy)
  resources :accounts do
    get :history, on: :member
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
