require 'test_helper'

class AccountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @account = accounts(:one)
    @owner = people(:one)
  end

  test "should get index" do
    get accounts_url, as: :json
    assert_response :success
  end

  test "should create account" do
    assert_difference('Account.count') do
      post accounts_url, params: { account: { name: @account.name, owner_id: @owner.id, owner_type: @owner.class.to_s } }, as: :json
    end

    assert_response 201
  end

  test "should show account" do
    get account_url(@account), as: :json
    assert_response :success
  end

  test "should update account" do
    patch account_url(@account), params: { account: { name: @account.name } }, as: :json
    assert_response 200
  end

  test "should cancel but not destroy account" do
    assert_no_difference('Account.count') do
      delete account_url(@account), as: :json
    end

    assert @account.reload.canceled?
    assert_response 204
  end

  test "should show account history" do
    get history_account_url(@account), as: :json
    assert_response :success
  end
end
