require 'test_helper'

class ChargesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @charge = charges(:one)
  end

  test "should create charge" do
    assert_difference('Charge.count') do
      post charges_url, params: { charge: { account_id: @charge.account_id, code: @charge.code, value: @charge.value } }, as: :json
    end

    assert_response 201
  end

  test "should show charge" do
    get charge_url(@charge), as: :json
    assert_response :success
  end

  test "should revert charge but not destroy it" do
    assert_no_difference('Charge.count') do
      delete charge_url(@charge, code: @charge.code), as: :json
    end

    assert_response 200
  end
end
