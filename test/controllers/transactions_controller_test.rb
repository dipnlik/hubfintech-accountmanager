require 'test_helper'

class TransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @transaction = transactions(:valid)
  end

  test "should create transaction" do
    assert_difference('Transaction.count') do
      post transactions_url, params: { transaction: { destination_account_id: @transaction.destination_account_id, source_account_id: @transaction.source_account_id, value: @transaction.value } }, as: :json
    end

    assert_response 201
  end

  test "should show transaction" do
    get transaction_url(@transaction), as: :json
    assert_response :success
  end

  test "should revert transaction but not destroy it" do
    assert_no_difference('Transaction.count') do
      delete transaction_url(@transaction), as: :json
    end

    assert_response 200
  end
end
