require 'test_helper'

class TransactionTest < ActiveSupport::TestCase
  test "changes account balances" do
    a1 = accounts(:affiliate_one)
    a2 = accounts(:affiliate_two)
    a1_initial_balance = a1.balance
    a2_initial_balance = a2.balance

    transacted_value = 1.99
    Transaction.create!(source_account: a1, destination_account: a2, value: transacted_value)
    assert_equal a1_initial_balance - transacted_value, a1.reload.balance
    assert_equal a2_initial_balance + transacted_value, a2.reload.balance
  end

  test "destination account must be active" do
    a1 = accounts(:affiliate_one)
    a2 = accounts(:affiliate_two)
    a2.update(status: 'blocked')

    transaction = Transaction.new(source_account: a1, destination_account: a2, value: 0.99)
    refute transaction.valid?
    assert_includes transaction.errors[:destination_account_id], 'must be active'
  end

  test "destination cannot be a main account" do
    a1 = accounts(:main_one)
    a2 = accounts(:affiliate_one)

    transaction = Transaction.new(source_account: a2, destination_account: a1, value: 0.99)
    transaction.valid?
    assert_includes transaction.errors[:destination_account_id], 'cannot be a main account'
  end

  test "destination must belong to the same tree as source account" do
    a1 = accounts(:affiliate_one)
    a2 = accounts(:affiliate_of_main_two)

    transaction = Transaction.new(source_account: a1, destination_account: a2, value: 0.99)
    transaction.valid?
    assert_includes transaction.errors[:destination_account_id], 'must belong to the same tree as source account'
  end

  test "can be reverted" do
    a1 = accounts(:affiliate_one)
    a2 = accounts(:affiliate_two)
    a1_initial_balance = a1.balance
    a2_initial_balance = a2.balance

    transaction = Transaction.create!(source_account: a1, destination_account: a2, value: 9.90)
    transaction.revert!
    assert_equal a1_initial_balance, a1.reload.balance
    assert_equal a2_initial_balance, a2.reload.balance
  end
end
