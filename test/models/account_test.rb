require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  test "belongs to parent account (optional)" do
    a1 = accounts(:one)
    a2 = accounts(:two)
    a2.update(parent_account: a1)

    assert_nil a1.parent_account
    assert_equal a1, a2.parent_account
  end

  test "has many affiliate accounts" do
    a1 = accounts(:one)
    a2 = accounts(:two)
    a2.update!(parent_account: a1)

    assert_equal 1, a1.affiliate_accounts.size
    assert_equal a2, a1.affiliate_accounts.first
  end

  test "main accounts are accounts without parent" do
    a1 = accounts(:one)
    a2 = accounts(:two)
    assert a1.main?
    assert a2.main?

    a2.update(parent_account: a1)
    refute a2.main?
  end

  test "main parent account id" do
    a1 = accounts(:one)
    a2 = accounts(:two)
    a3 = accounts(:three)
    a2.update(parent_account: a1)
    a3.update(parent_account: a2)

    assert_equal a1.id, a1.main_parent_account_id
    assert_equal a1.id, a2.main_parent_account_id
    assert_equal a1.id, a3.main_parent_account_id
  end

  test "must have a person or a company" do
    account = Account.new
    refute account.valid?
    assert_equal 1, account.errors[:owner].size

    account.owner = people(:one)
    account.valid?
    assert_equal 0, account.errors[:owner].size

    account.owner = companies(:one)
    account.valid?
    assert_equal 0, account.errors[:owner].size

    account.owner = accounts(:one)
    refute account.valid?
    assert_equal 1, account.errors[:owner_type].size
  end

  test "has a history of operations" do
    main_account = accounts(:main_one)
    a1 = accounts(:affiliate_one)
    a2 = accounts(:affiliate_two)

    Transaction.delete_all; Charge.delete_all   # ensure fixtures won't interfere

    # FIXME this code block should not need sleep calls
    #   it looks like created_at precision is being lost somewhere, this breaks sorting
    c1 = Charge.create!(account: main_account, value: 20)
    t1 = Transaction.create!(source_account: main_account, destination_account: a1, value: 8)
    sleep 1
    t2 = Transaction.create!(source_account: main_account, destination_account: a2, value: 7)
    sleep 1
    c2 = Charge.create!(account: main_account, value: 10)
    t3 = Transaction.create!(source_account: a1, destination_account: a2, value: 0.99)

    assert_equal [c2, c1], main_account.deposits
    assert_equal [t1], a1.deposits
    assert_equal [t3, t2], a2.deposits

    assert_equal [t2, t1], main_account.withdrawals
    assert_equal [t3], a1.withdrawals
    assert_equal [], a2.withdrawals

    assert_equal [c2, t2, t1, c1], main_account.history
    assert_equal [t3, t1], a1.history
    assert_equal [t3, t2], a2.history
  end
end
