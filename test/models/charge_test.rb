require 'test_helper'

class ChargeTest < ActiveSupport::TestCase
  test "generates a unique code" do
    a1 = accounts(:one)
    charge = Charge.create!(account: a1, value: 0.99)
    refute_empty charge.reload.code
  end

  test "changes account balance" do
    a1 = accounts(:one)
    a1_initial_balance = a1.balance

    Charge.create!(account: a1, value: 1.99)
    assert_equal a1_initial_balance + 1.99, a1.reload.balance
  end

  test "account must be active" do
    a1 = accounts(:one)
    a1.update(status: 'blocked')

    charge = Charge.new(account: a1, value: 0.99)
    refute charge.valid?
    assert_includes charge.errors[:account_id], 'must be active'
  end

  test "can be reverted if provided with the correct code" do
    a1 = accounts(:one)
    a1_initial_balance = a1.balance

    charge = Charge.create!(account: a1, value: 0.99)
    a1_updated_balance = a1.balance

    charge.revert!('abc123')
    assert_equal a1_updated_balance, a1.reload.balance

    charge.revert!(charge.code)
    assert_equal a1_initial_balance, a1.reload.balance
  end
end
