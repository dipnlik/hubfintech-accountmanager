# hubfintech-accountmanager

# Configuração e execução

Desenvolvido usando Ruby 2.3.5 e MySQL 5.7.20 no macOS Sierra.  Não adicionei o ruby no `Gemfile` para não criar muitas restrições.

`bin/setup` deve ser suficiente pra configuração.

`bin/rails server` sobe a aplicação e a disponibiliza em http://localhost:3000.

`bin/rails test` roda a suite de testes.

# Notas

- `Person` e `Company` podiam herdar de uma mesma classe mas neste caso não achei que precisava tanto.  O mesmo vale para `Transaction` e `Charge`.
- Não sei se estes nomes são traduções adequadas de pessoa física, pessoa jurídica, transação e aporte, mas espero que o inglês não esteja muuuuito vergonhoso.  Se ajuda em algo, cogitei usar `LegalPerson`, `JuridicPerson`, `Transaction` e `Contribution`, respectivamente.
- Tudo foi gerado usando _scaffolds_ para depois ser feita uma limpeza, mas não acho que tenha sido um bom caminho, tive impressão que a limpeza deu muito trabalho.
- Provavelmente tem algum jeito melhor de armazenar as transações e aportes que facilite a consulta de histórico.
- Não sei como seria melhor a parte de `dependent: destroy` entre uma conta e suas transações.  Aportes estão como `dependent: destroy` porque se relacionam com uma única conta, mas transações envolvem duas contas.

# TODO

- Impedir múltiplos estornos
- Frontend
- Aprender `jbuilder` pra melhorar as respostas (`accounts/:id/history` podia incluir as associações, por exemplo)
- Documentar a API: tem `bin/rails routes` mas seria bom ter um swagger ou algo do tipo
- Ligar CI no Gitlab?
