class CreateCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :companies do |t|
      t.string :cnpj
      t.string :legal_name
      t.string :business_name

      t.timestamps
    end
  end
end
