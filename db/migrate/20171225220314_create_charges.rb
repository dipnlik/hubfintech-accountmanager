class CreateCharges < ActiveRecord::Migration[5.1]
  def change
    create_table :charges do |t|
      t.references :account, foreign_key: true
      t.decimal :value, precision: 10, scale: 2
      t.string :code

      t.timestamps
    end
  end
end
