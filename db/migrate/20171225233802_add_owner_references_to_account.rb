class AddOwnerReferencesToAccount < ActiveRecord::Migration[5.1]
  def change
    add_reference :accounts, :owner, polymorphic: true, index: true
  end
end
