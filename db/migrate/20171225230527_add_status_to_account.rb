class AddStatusToAccount < ActiveRecord::Migration[5.1]
  def change
    add_column :accounts, :status, :string, default: :active
  end
end
