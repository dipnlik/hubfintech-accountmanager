class CreatePeople < ActiveRecord::Migration[5.1]
  def change
    create_table :people do |t|
      t.string :cpf
      t.string :name
      t.date :birth_date

      t.timestamps
    end
  end
end
