# Taken from account_test.rb

dip = Person.create!(name: 'Alexandre Lima')
main_account = Account.create!(name: "Alexandre Lima's main account", owner: dip)
a1 = Account.create!(name: "Conta Corrente do dip", owner: dip, parent_account: main_account)
a2 = Account.create!(name: "Conta Poupança do dip", owner: dip, parent_account: main_account)

c1 = Charge.create!(account: main_account, value: 20)
t1 = Transaction.create!(source_account: main_account, destination_account: a1, value: 8)
sleep 1
t2 = Transaction.create!(source_account: main_account, destination_account: a2, value: 7)
sleep 1
c2 = Charge.create!(account: main_account, value: 10)
t3 = Transaction.create!(source_account: a1, destination_account: a2, value: 0.99)
