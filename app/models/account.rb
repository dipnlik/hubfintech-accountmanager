class Account < ApplicationRecord
    has_many :affiliate_accounts, class_name: 'Account', foreign_key: 'parent_id'
    belongs_to :parent_account, class_name: 'Account', foreign_key: 'parent_id', optional: true
    has_many :charges, dependent: :destroy
    has_many :incoming_transactions, class_name: 'Transaction', foreign_key: 'destination_account_id'
    has_many :outgoing_transactions, class_name: 'Transaction', foreign_key: 'source_account_id'
    belongs_to :owner, polymorphic: true

    validates :status, inclusion: { in: %w(active blocked canceled) }
    validates :owner_type, inclusion: { in: %w(Person Company) }

    def active?
        self.status == 'active'
    end

    def main?
        self.parent_id.nil?
    end

    def main_parent_account_id
        result = self.id
        current_parent = parent_account
        while current_parent
            result = current_parent.id
            current_parent = current_parent.parent_account
        end
        result
    end

    def deposit(value)
        self.balance += value
        self.save!
    end

    def withdraw(value)
        self.balance -= value
        self.save!
    end

    def deposits
        list = self.charges.order(created_at: :desc) + self.incoming_transactions.order(created_at: :desc)
        list.sort_by(&:created_at).reverse
    end

    def withdrawals
        self.outgoing_transactions.order(created_at: :desc)
    end

    def history
        list = self.deposits + self.withdrawals
        list.sort_by(&:created_at).reverse
    end

    def cancel!
        self.update(status: 'canceled')
    end

    def canceled?
        self.status == 'canceled'
    end
end
