require 'securerandom'

class Charge < ApplicationRecord
  belongs_to :account

  validates :account, presence: true
  validates :value, numericality: { greater_than: 0.0 }
  validate :account_must_be_active

  before_create :generate_code
  after_create :perform!
  
  def revert!(code)
    account.withdraw(value) if self.code == code
  end

  private

  def generate_code
    self.code = SecureRandom.uuid
  end

  def perform!
    account.deposit(value)
  end

  def account_must_be_active
    errors.add(:account_id, :must_be_active) unless account.active?
  end
end
