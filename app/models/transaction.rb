class Transaction < ApplicationRecord
    belongs_to :source_account, class_name: 'Account', foreign_key: 'source_account_id'
    belongs_to :destination_account, class_name: 'Account', foreign_key: 'destination_account_id'

    validates :source_account, :destination_account, presence: true
    validates :value, numericality: { greater_than: 0.0 }

    validate :destination_cannot_be_a_main_account,
        :destination_must_belong_to_the_same_tree_as_source,
        :destination_must_be_active

    after_create :perform!

    def revert!
        transaction do
            destination_account.withdraw(value)
            source_account.deposit(value)
        end
    end

    private

    def perform!
        transaction do
            source_account.withdraw(value)
            destination_account.deposit(value)
        end
    end

    def destination_cannot_be_a_main_account
        errors.add(:destination_account_id, :cannot_be_a_main_account) if destination_account&.main?
    end

    def destination_must_belong_to_the_same_tree_as_source
        if destination_account.main_parent_account_id != source_account.main_parent_account_id
            errors.add(:destination_account_id, :must_belong_to_the_same_tree_as_source)
        end
    end

    def destination_must_be_active
        errors.add(:destination_account_id, :must_be_active) unless destination_account.active?
    end
end
