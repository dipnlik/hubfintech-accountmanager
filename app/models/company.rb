class Company < ApplicationRecord
    has_many :accounts, as: :owner, dependent: :restrict_with_exception
end
