class ChargesController < ApplicationController
  before_action :set_charge, only: [:show, :destroy]

  # GET /charges/1
  def show
    render json: @charge
  end

  # POST /charges
  def create
    @charge = Charge.new(charge_params)

    if @charge.save
      render json: @charge, status: :created, location: @charge
    else
      render json: @charge.errors, status: :unprocessable_entity
    end
  end

  # DELETE /charges/1
  def destroy
    if @charge.revert! params[:code]
      render json: @charge, status: :ok, location: @charge
    else
      render json: @charge.errors, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_charge
      @charge = Charge.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def charge_params
      params.require(:charge).permit(:account_id, :value, :code)
    end
end
